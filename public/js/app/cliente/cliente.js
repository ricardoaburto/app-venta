App.Dom.init({

    prompDeleteMessage: "¿Esta seguro que desea eliminar esta Cliente?",
    deleteMessageSuccess: "Se han eliminado correctamente el Cliente",
    deleteMessageFail: "Ha Ocurrido un error eliminando el Cliente",
    onOpenFormCallback: function () {
    },
    onPreOpenFormCallback: function () {
    },
    onInitCompleteCallback: function () {

    }
});

//carga datatable
App.oTable = $('[data-tablemain]').dataTable({
    responsive: true,
    "oLanguage": {
        "sUrl": App.BASE_URL + "/js/datatables-es.json"
    },
    "aaSorting": [[0, "desc"]],
    "columnDefs": [
        {"orderable": false, "targets": [1]},
    ],
    "bProcessing": true,
    "bServerSide": true,
    "bRetrieve": true,
    "sAjaxSource": App.BASE_URL + "/app/cliente/table_cliente",
    "fnInitComplete": function (oSettings, json) {
        $(".dataTables_filter").addClass('pull-right');
        $("#primal-table_paginate").addClass('pull-right');
    }
});

$("#primal-form").validate({
    rules: {
        nombre: {
            required: true
        }
    },
    messages: {
        nombre: {
            required: "Debe ingresar un(a) cliente"
        }
    },
    highlight: function (element) {
        $(element).closest('.form-group .bs-component').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group .bs-component').removeClass('has-error').find('.bs-component label.error').remove();
    },
    errorPlacement: function (error, element) {
        if ($(element).is('select.chosen-select')) {
            $(element).closest(' .bs-component').append(error);
        } else {
            element.after(error);
        }
    },
    submitHandler: function (form) {

        var dataForm = $(form).serialize();
        if ($(".pk_form").val() === '0') {
            blackout.show("Guardando...", function () {
                $.ajax({
                    type: "POST",
                    url: App.BASE_URL + "/app/cliente/create",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: dataForm
                }).done(function (data) {
                    setTimeout(function () {
                        try {
                            var r = JSON.parse(data);
                            if (r.response === 1) {
                                App.oTable.fnDraw();
                                blackout.hide(function () {
                                    App.Dom.closeForm();
                                    $.gritter.add({
                                        title: "OK!",
                                        text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                        class_name: 'growl-success',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        } catch (e) {
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "Error",
                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                    class_name: 'growl-danger',
                                    sticky: false,
                                    time: 2000
                                });
                            });
                        }
                    }, 1);
                }).error(function () {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                });
            });
        } else {
            blackout.show("Guardando...", function () {
                $.ajax({
                    type: "POST",
                    url: App.BASE_URL + "/app/cliente/actualizar",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: dataForm
                }).done(function (data) {
                    try {
                        var r = JSON.parse(data);
                        setTimeout(function () {
                            if (r.response === 1) {
                                blackout.hide(function () {
                                    App.oTable.fnDraw();
                                    App.Dom.closeForm();
                                    $.gritter.add({
                                        title: "OK!",
                                        text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                        class_name: 'growl-success',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        }, 1);
                    } catch (e) {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                }).error(function () {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                });
            });
        }
    }
});

App.editar = function (idpk) {
    $('table#tabla_venta tbody').empty();
    App.Dom.editStandard({
        url: App.BASE_URL + "/app/cliente/" + idpk + "/edit",
        callback: function (data) {
            $('[name=idcliente]').val(idpk);
            $('[name=nombre]').val(data.data.data.nombre);
        }
    });
};

App.eliminar = function (idpk) {
    App.Dom.closeForm(function () {

        xmodal.show(null, App.Dom.prompDeleteMessage, function () {
        blackout.show('Espere...', function () {
            $.ajax({
                type: "POST",
                url: App.BASE_URL + "/app/cliente/eliminar",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: "id=" + idpk
            }).done(function (data) {
                try {
                    var r = JSON.parse(data);
                    if (r.response === 1) {
                        if(parseInt(r.data.data) ===0){
                            App.oTable.fnDraw();

                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "OK!",
                                    text: "<i class='fa fa-clock-o'></i> <i>Se han eliminado correctamente</i>",
                                    class_name: 'growl-success',
                                    sticky: false,
                                    time: 2000
                                });
                            });
                        }else{
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "INFO!",
                                    text: "<i class='fa fa-clock-o'></i> <i>No de puede eliminar el producto esta asignado a un detalle</i>",
                                    class_name: 'growl-info',
                                    sticky: false,
                                    time: 2000
                                });
                            });
                        }

                    } else {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando la Venta</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                    xmodal.hide();
                } catch (e) {

                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>" + App.Dom.deleteMessageFail + "</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    });
                    xmodal.hide();
                }
            }).error(function () {

                blackout.hide(function () {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>" + App.Dom.deleteMessageFail + "</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                });
                xmodal.hide();
            });
        });
        });

    });
};
