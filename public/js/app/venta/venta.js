App.Dom.init({
    prompDeleteMessage: "¿Esta seguro que desea eliminar esta Venta?",
    deleteMessageSuccess: "Se han eliminado correctamente el Venta",
    deleteMessageFail: "Ha Ocurrido un error eliminando el Venta",
    onOpenFormCallback: function () {
    },
    onPreOpenFormCallback: function () {
        $('table#tabla_venta tbody').empty();
        $('table#tabla_venta tbody').append(htmlInit);
        $('[name=subtotal]').text(0);
        $('[name=iva]').text(0);
        $('[name=total]').text(0);
        $('[name=descuento]').val(0);
    },
    onInitCompleteCallback: function () {
        $('[name=precio]').val(0);
        App.getSelectInit();
        $('[name=cantidad]').keypress(function (tecla) {
            if (tecla.charCode < 48 || tecla.charCode > 57) return false;
        });
        $('[name=descuento]').keypress(function (tecla) {
            if (tecla.charCode < 48 || tecla.charCode > 57) return false;
        });
        $('[name=precio]').prop('disabled', true);
    }
});
//carga datatable
App.oTable = $('[data-tablemain]').dataTable({
    responsive: true,
    "oLanguage": {
        "sUrl": App.BASE_URL + "/js/datatables-es.json"
    },
    "aaSorting": [[0, "desc"]],
    "columnDefs": [
        { "orderable": false, "targets": [3] },
    ],
    "aoColumnDefs": [
        {"sClass": 'text-center', 'aTargets': [3]},
    ],
    "bProcessing": true,
    "bServerSide": true,
    "bRetrieve": true,
    "sAjaxSource": App.BASE_URL + "/app/table_venta",
    "fnInitComplete": function (oSettings, json) {
        $(".dataTables_filter").addClass('pull-right');
        $("#primal-table_paginate").addClass('pull-right');
    }
});

//init de datos al cargar el formulario
App.getSelectInit = function () {
    $.ajax({
        type: "GET",
        url: App.BASE_URL + "/app/venta/get"
    }).done(function (data) {
        var r = JSON.parse(data);
        if (r.response === 1) {
            $('[name=idcliente]').append(r.data.cliente).trigger('chosen:updated');
            $('[name=idproducto]').append(r.data.producto).trigger('chosen:updated');
        }

    });
}

$('[name=idproducto]').bind('change', function () {
    App.getValorProducto($('[name=idproducto]').val());
});

App.getValorProducto = function (id) {
    $.ajax({
        type: "POST",
        url: App.BASE_URL + "/app/venta/getvalue",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data: {
            "id": id
        },
    }).done(function (data) {
        var r = JSON.parse(data);
        if (r.response === 1) {
            $('[name=precio]').val(r.data.data).trigger('change');
            $('[name=cantidad]').val(0);
        } else {
            $.gritter.add({
                title: "Error",
                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                class_name: 'growl-danger',
                sticky: false,
                time: 2000
            });
        }

    });
}

var contador = 0;
App.addProducto = function () {
    var existeValoCero = 0;
    $('table#tabla_venta  tbody tr').each(function () {
        if ($(this).attr('data-id') === "0") {
            existeValoCero = 1;
        }
    });

    if ($('[name=idproducto]').val() === null) {
        new TM('info', 'Debe Seleccionar un Producto').show();
    } else if ($('[name=precio]').val() === "" || parseInt($('[name=precio]').val()) === 0) {
        new TM('info', 'Debe ingresar una Precio válido').show();
    } else if ($('[name=cantidad]').val() === "" || $('[name=cantidad]').val() === "0") {
        new TM('info', 'Debe ingresar una Cantidad válida').show();
    } else {
        var suma=0;
        $('table#tabla_venta tbody tr').each(function () {
            if(parseInt($(this).attr('data-id')) !==0){
                suma+=parseInt($(this).attr('data-cantidad'));
            }
        });
        $.ajax({
            type: "POST",
            url: App.BASE_URL + "/app/venta/detalle/add",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                "idproducto": $('[name=idproducto]').val(),
                "cantidad": parseInt($('[name=cantidad]').val()),
                "total_cantidad":(parseInt($('[name=cantidad]').val()) + suma),
                "precio": $('[name=precio]').val(),
                "contador": contador
            },
        }).done(function (data) {
            var r = JSON.parse(data);
            if (r.response === 1) {
                if(parseInt(r.data.stock) === 1){
                    if (existeValoCero === 1) {
                            $('table#tabla_venta tbody').empty();
                        }
                    $('table#tabla_venta tbody').append(r.data.data);
                    App.cantidad();
                    $.gritter.add({
                        title: "OK!",
                        text: "<i class='fa fa-clock-o'></i> <i>Se han agregado correctamente el producto</i>",
                        class_name: 'growl-success',
                        sticky: false,
                        time: 2000
                    });
                    contador++;

                }else{
                    var mensaje="No queda stock de Productos";
                    if(parseInt(r.data.stockDisponible) !== 0){
                        mensaje ="Puede agregar "+r.data.stockDisponible+" producto maximo";
                      }
                    $.gritter.add({
                        title: "Info",
                        text: "<i class='fa fa-clock-o'></i> <i>"+mensaje+"</i>",
                        class_name: 'growl-info',
                        sticky: false,
                        time: 2000
                    });
                }

            } else {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
            }
        });
    }
}

$("#primal-form").validate({
    rules: {
        idcliente: {
            required: true
        }
    },
    messages: {
        idcliente: {
            required: "Debe ingresar un(a) Cliente"
        }
    },
    highlight: function (element) {
        $(element).closest('.form-group .bs-component').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group .bs-component').removeClass('has-error').find('.bs-component label.error').remove();
    },
    errorPlacement: function (error, element) {
        if ($(element).is('select.chosen-select')) {
            $(element).closest(' .bs-component').append(error);
        } else {
            element.after(error);
        }
    },
    submitHandler: function (form) {
        $('[name=precio]').prop('disabled', false);
        var cant = 0;
        var a = [];
        $('table#tabla_venta tbody tr').each(function () {
            if (0 === parseInt($(this).attr('data-id'))) {
                cant = 1;
            }
            a.push({
                'producto_id': parseInt($(this).attr('data-idproducto')),
                'cantidad': parseInt($(this).attr('data-cantidad'))
            });
        });
        var dataForm = $(form).serialize() + "&arrayVenta=" + JSON.stringify(a) + "&iva=" +$('[name=iva]').text()  + "&total=" + $('[name=total]').text() + "&subtotal=" + $('[name=subtotal]').text();
        if (cant === 0) {
            if ($(".pk_form").val() === '0') {
                blackout.show("Guardando...", function () {
                    $.ajax({
                        type: "POST",
                        url: App.BASE_URL + "/app/venta/create",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: dataForm
                    }).done(function (data) {
                        $('#bloqueo_ajax').prop('disabled', true).trigger('chosen:updated');
                        setTimeout(function () {
                            try {
                                var r = JSON.parse(data);
                                if (r.response === 1) {
                                    App.oTable.fnDraw();
                                    blackout.hide(function () {
                                        App.Dom.closeForm();
                                        $.gritter.add({
                                            title: "OK!",
                                            text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                            class_name: 'growl-success',
                                            sticky: false,
                                            time: 2000
                                        });
                                    });
                                } else {
                                    blackout.hide(function () {
                                        $.gritter.add({
                                            title: "Error",
                                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                            class_name: 'growl-danger',
                                            sticky: false,
                                            time: 2000
                                        });
                                    });
                                }
                            } catch (e) {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        }, 1);
                    }).error(function () {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 4000
                            });
                        });
                    });
                });
            } else {
                blackout.show("Guardando...", function () {
                    $.ajax({
                        type: "POST",
                        url: App.BASE_URL + "/app/venta/actualizar",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: dataForm
                    }).done(function (data) {
                        try {
                            var r = JSON.parse(data);
                            setTimeout(function () {
                                if (r.response === 1) {
                                    blackout.hide(function () {
                                        App.oTable.fnDraw();
                                        App.Dom.closeForm();
                                        $.gritter.add({
                                            title: "OK!",
                                            text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                            class_name: 'growl-success',
                                            sticky: false,
                                            time: 2000
                                        });
                                    });
                                } else {
                                    blackout.hide(function () {
                                        $.gritter.add({
                                            title: "Error",
                                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                            class_name: 'growl-danger',
                                            sticky: false,
                                            time: 2000
                                        });
                                    });
                                }
                            }, 1);
                        } catch (e) {
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "Error",
                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                    class_name: 'growl-danger',
                                    sticky: false,
                                    time: 2000
                                });
                            });
                        }
                    }).error(function () {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 4000
                            });
                        });
                    });
                });
            }
        } else {
            $.gritter.add({
                title: "Info",
                text: "<i class='fa fa-clock-o'></i> <i>Debe agregar al menos un Producto</i>",
                class_name: 'growl-danger',
                sticky: false,
                time: 2000
            });
        }
    }
});

App.editar = function (idpk) {
    $('table#tabla_venta tbody').empty();
    App.Dom.editStandard({
        url: App.BASE_URL + "/app/venta/" + idpk + "/edit",
        callback: function (data) {
            $('[name=idventa]').val(idpk);
            var tabla = data.data.tabla;
            var descuento = data.data.descuento;
            var iva = data.data.iva;
            var subtotal = data.data.subtotal;
            var total = data.data.total;
            var cliente = data.data.cliente;
            $('[name=idcliente]').val(cliente).trigger('chosen:updated');
            $('table#tabla_venta tbody').append(tabla);
            if(parseInt(descuento)!== 0){
            $('[name=descuento]').val(descuento).trigger('change');
            }else{
                $('[name=subtotal]').text(subtotal);
                $('[name=descuento]').val(descuento);
                $('[name=iva]').text(iva);
                $('[name=total]').text(total);
            }
        }
    });
};

App.eliminar = function (idpk) {
    App.Dom.deleteStandard({
        id: idpk,
        url: App.BASE_URL + "/app/venta/eliminar",
    });
};

//Elimina detalle solo de la tabla
App.eliminarDetalle = function (id) {
    var cantidadTr = 0;
    if ($('table#tabla_venta tbody tr').length === 1) {
        $('table#tabla_venta tbody tr').each(function () {
            if (0 !== $(this).attr('data-id')) {
                cantidadTr = 1;
            }
        });
    }
    $('table#tabla_venta tbody tr').each(function () {
        if (id == $(this).attr('data-id')) {
            $(this).remove();
        }
    });
    if (cantidadTr === 1) {
        $('table#tabla_venta tbody').append(htmlInit);
        $('[name=subtotal]').text(0);
        $('[name=iva]').text(0);
        $('[name=total]').text(0);
        $('[name=descuento]').val(0);
    } else {
        App.cantidad();
    }
}

App.cantidad = function () {
    var des = $('[name=descuento]').val() !== "" || parseInt($('[name=descuento]').val()) > 0 ? parseInt($('[name=descuento]').val()) : 0;
    var subTotal = 0;
    $('table#tabla_venta tbody tr').each(function () {
        subTotal += parseInt($(this).attr('data-precio')) * parseInt($(this).attr('data-cantidad'));
    });
    if (parseInt($('[name=descuento]').val()) > 0) {
        var descuento=(subTotal * des) / 100
        var sub = (subTotal - descuento) ;
        var iva = sub * 0.19;
        var total = sub + iva ;
        $('[name=iva]').text(iva);
        $('[name=subtotal]').text(subTotal);
        $('[name=total]').text(total);
    } else {
        var iva = subTotal * 0.19;
        $('[name=subtotal]').text(subTotal);
        $('[name=iva]').text((subTotal * 0.19));
        $('[name=total]').text((subTotal + iva));
    }
}

$('[name=descuento]').bind('change', function () {
    if(this.value>100){this.value='100';}else if(this.value<0){this.value='0';}
    App.cantidad();
})


App.setNumberFormat = function () {
    var el = $(this);
    let n = extract_number(el.val(), 0);
    el.val(number_format(n, 0, ',', '.'));
};

App.pdf = function (id) {
    window.open(App.BASE_URL + "/app/venta/pdf-boleta/"+id);
}
