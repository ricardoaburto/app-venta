/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var blackout = function () {
    var loadInnerBlackout = function () {
        var bot = document.getElementById('BlackOutHandler');
        bot.innerHTML = '<div class="container-fluid">\n\
                            <div class="spinner">\n\
                                <span class="ball-1"></span>\n\
                                <span class="ball-2"></span>\n\
                                <span class="ball-3"></span>\n\
                                <span class="ball-4"></span>\n\
                            </div>\n\
                            <div class="textloader">Cargando...</div>\n\
                        </div>';
    };

    return {
        init: function () {
            if (!document.getElementById('BlackOutHandler')) {
                var body = document.getElementsByTagName('body')[0];
                var bo = document.createElement('div');
                bo.id = 'BlackOutHandler';
                body.appendChild(bo);
                bo.onload = loadInnerBlackout();
            } else {
                loadInnerBlackout();
            }
            return true;
        },
        show: function (text, callback) {
            text = (text) ? text : 'Cargando...';
            callback = (callback) ? callback : function () {
            };
            var $t = $('#BlackOutHandler');
            if (typeof text === "string") {
                $t.find('.textloader').html(text);
                if (typeof callback === "function") {
                    $t.fadeIn(callback());
                } else {
                    $t.fadeIn();
                }
            } else if (typeof text === "function") {
                $t.fadeIn(function () {
                    callback();
                });
            } else {
                $t.fadeIn();
            }
        },
        hide: function (callback) {
            callback = (callback) ? callback : function () {
            };
            var $t = $('#BlackOutHandler');
            if (typeof callback === "function") {
                $t.fadeOut(function () {
                    callback();
                });
            } else {
                console.log('b');
                $t.fadeOut();
            }
        }
    };
}();

var misc = function () {
    return {
        today: function () {
            var f = new Date();
            var dia = f.getDate();
            dia = (parseInt(dia) < 10) ? "0" + dia : dia;
            return (f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + dia);
        },
        now: function () {
            var f = new Date();
            var mins = '00';
            var hours = '00';
            mins = (f.getMinutes()<10)?'0'+f.getMinutes():f.getMinutes();
            hours = (f.getHours()<10)?'0'+f.getHours():f.getHours();
            return (hours + ':' + mins);
        },
        STR_PAD_LEFT: 1,
        STR_PAD_RIGHT: 2,
        STR_PAD_BOTH: 3,
        pad: function (str, len, pad, dir) {

            if (typeof (len) == "undefined") {
                var len = 0;
            }
            if (typeof (pad) == "undefined") {
                var pad = ' ';
            }
            if (typeof (dir) == "undefined") {
                var dir = misc.STR_PAD_LEFT;
            }

            if (len + 1 >= str.length) {

                switch (dir) {

                    case misc.STR_PAD_LEFT:
                        str = Array(len + 1 - str.length).join(pad) + str;
                        break;

                    case misc.STR_PAD_BOTH:
                        var right = Math.ceil((padlen = len - str.length) / 2);
                        var left = padlen - right;
                        str = Array(left + 1).join(pad) + str + Array(right + 1).join(pad);
                        break;

                    default:
                        str = str + Array(len + 1 - str.length).join(pad);
                        break;

                } // switch

            }

            return str;

        }
    };
}();