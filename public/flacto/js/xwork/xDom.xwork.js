/* global JSONTRUE, blackout, BASE_URL_JS, xmodal */

/**
 * Copyright ©2014-2016 by Marcel Rojas
 */
'use strict';

/**
 * Dom form XWork
 * @returns {Dom}
 */
var Dom = function () {
    this.daemonInterval = 1;
    this.Xconf = function (conf) {
        this.hreset();
        for (var i in conf) {
            this[i] = conf[i];
        }
    };
};

Dom.prototype.onLaunchAppRun = function () {
    $(function () {
        setTimeout(function () {
            $("#xMainMenu li.active>a[data-isdeadendlink]").parentsUntil($("#xMainMenu"), 'li').each(function () {
                $(this).find('a:not([data-isdeadendlink])').click();
            });
        }, 102);
    });
};

Dom.prototype.init = function (xconfig) {
    this.Xconf(xconfig);
    App.oTable = null;
    var config = {
        '.chosen-select': {'min-width': '100px', 'white-space': 'nowrap', no_results_text: 'No Existen Resultados!'},
        '.chosen-select-deselect': {allow_single_deselect: true, no_results_text: 'No Existen Resultados!'},
        '.chosen-select-no-single': {disable_search_threshold: 10, no_results_text: 'No Existen Resultados!'},
        '.chosen-select-no-results': {no_results_text: 'No Existen Resultados!'},
        '.chosen-select-width': {width: "95%", no_results_text: 'No Existen Resultados!'}
    };

    $("[class*=chosen-select]").bind('change', function () {
        if ($(this).parents('form').length) {
            $(this).valid();
        }
    });

    // if ($.chosen) {
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    // }
    setTimeout(function () {
        $(".chosen-container").attr('style', 'width:100%');
    }, 100);


    $("[data-addnew]").bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        App.Dom.openForm();
    });

    $("[data-cancel_form]").bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (App.Dom.onCancelFormCallback && typeof App.Dom.onCancelFormCallback === "function") {
            App.Dom.onCancelFormCallback();
        }
        App.Dom.closeForm();
    });

    $("[data-rut]").Rut({
        format_on: 'keyup'
    });
    this.validationExtras();
    this.translateDatePickerAndTrigger();
    setTimeout(function () {
        if (App.Dom.onInitCompleteCallback && typeof App.Dom.onInitCompleteCallback === "function") {
            App.Dom.onInitCompleteCallback();
        }
    }, 101);
};

Dom.prototype.hreset = function () {
    this.onCancelFormCallback = null;
    this.onInitCompleteCallback = null;
    this.onRefreshFormCallback = null;
    this.onCloseFormCallback = null;
    this.onPreOpenFormCallback = null;
    this.onOpenFormCallback = null;
    this.onSwitchChangeCallback = null;
//    runall();
};

Dom.prototype.translateDatePickerAndTrigger = function () {
    var xdateElement = $('[data-xdatepicker]');
    if (navigator.userAgent.indexOf('Chrome') != -1) {
        xdateElement.on('click', function (event) {
            event.preventDefault();
        });
    }

    try {
        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '<Ant',
            nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);

//        var x = '2016-10-20';
        var x = $('[name=fecha_inicio_contrato]').val();
        var fex = x.split('-');
        var aniox = fex[0];
        var mx = fex[1];
        var m_x = mx.split('');
        if (parseInt(m_x[0]) > 0) {
            var mesx = m_x[0] + m_x[1];
        } else {
            mesx = m_x[1];
        }
        var dx = fex[2];
        var d_x = dx.split('');
        if (parseInt(d_x[0]) > 0) {
            var diax = parseInt(d_x[0] + d_x[1]);
        } else {
            diax = parseInt(d_x[1]);
        }

        var y = $('[name=fecha_termino_contrato]').val();
        var fey = y.split('-');
        var anioy = fey[0];
        var my = fey[1];
        var m_y = my.split('');
        if (parseInt(m_y[0]) > 0) {
            var mesy = m_y[0] + m_y[1];
        } else {
            mesy = m_y[1];
        }
        var dy = fey[2];
        var d_y = dy.split('');
        if (parseInt(d_y[0]) > 0) {
            var diay = parseInt(d_y[0] + d_y[1]);
        } else {
            diay = parseInt(d_y[1]);
        }
//        alert(dia);
        xdateElement.each(function () {
            var $o = $(this);
            var fnc = $o.attr('data-xdatepicker');
            $o.datepicker({
                language: 'es',
                dateFormat: 'yy-mm-dd',
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                minDate: new Date(aniox, mesx - 1, diax),
                maxDate: new Date(anioy, mesy - 1, diay),
                onSelect: function (selectedDate) {
                    try {
                        if (fnc !== "") {
                            App.Dom.executeFunctionByName(fnc, window, selectedDate);
                        }
                    } catch (e) {

                    }
                }
            });
        });
    } catch (e) {

    }
};

Dom.prototype.executeFunctionByName = function (functionName, context /*, args */) {
    try {
        var args = [].slice.call(arguments).splice(2);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        for (var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        return context[func].apply(context, args);
    } catch (e) {

    }
};

Dom.prototype.refreshForm = function (form, callback) {
    form = (form) ? form : '#primal-form';
    if ($(form)[0]) {
        $(form)[0].reset();
    }
//    $(form + ' .i-checks').iCheck('uncheck');
    $(form + ' select').val(null).trigger('chosen:updated');
    $(form).find(':input').closest('.bs-component').removeClass('has-error').find('label.error').remove();
    $(form).find(':input').closest('.form-control').removeClass('has-error').find('label.error').remove();
    $(form).find(':input').closest('.form-group').removeClass('has-error').find('.bs-component label.error').remove();
    if (App.Dom.onRefreshFormCallback && typeof App.Dom.onRefreshFormCallback === "function") {
        App.Dom.onRefreshFormCallback();
        if (callback && typeof callback === "function") {
            callback();
        }
    }
    if (form === "#primal-form") {
        $(".pk_form").val(0);
    }
};

Dom.prototype.closeForm = function (callback) {
    var f = $('[data-maincontainerform]');
    if (f.length > 0) {
        f.slideUp(function () {
            App.Dom.refreshForm();
            if (App.Dom.onCloseFormCallback && typeof App.Dom.onCloseFormCallback === "function") {
                App.Dom.onCloseFormCallback();
            }
            if (typeof callback === "function") {
                callback();
            }
        });
    } else {
        if (typeof callback === "function") {
            callback();
        }
    }
};

Dom.prototype.openForm = function () {
    if (App.Dom.onPreOpenFormCallback && typeof App.Dom.onPreOpenFormCallback === "function") {
        App.Dom.onPreOpenFormCallback();
    }
    if ($("#primal-form")) {
        App.Dom.closeForm();
        setTimeout(function () {
            App.Dom.refreshForm();
            $('.only_on_edit').hide();
            $('.form-title').html((typeof App.Dom.formTitleAdd !== "undefined") ? App.Dom.formTitleAdd : "Agregar");
            setTimeout(function () {
                var hoy = new Date();
                var mes = (((hoy.getMonth() + 1) < 10) ? "0" + (hoy.getMonth() + 1) : (hoy.getMonth() + 1));
                $('[data-datetoday]').val((hoy.getFullYear() + "-" + mes + "-" + hoy.getDate()));
                $('[data-maincontainerform]').slideDown(function () {
                    if (App.Dom.onOpenFormCallback && typeof App.Dom.onOpenFormCallback === "function") {
                        App.Dom.onOpenFormCallback();
                    }
                });
            }, 10);
        }, 400);
    } else {
        App.Dom.refreshForm();
        $('.only_on_edit').hide();
        $('.form-title').html((typeof App.Dom.formTitleAdd !== "undefined") ? App.Dom.formTitleAdd : "Agregar");
        setTimeout(function () {
            $('[data-maincontainerform]').slideDown(function () {
                if (App.Dom.onOpenFormCallback && typeof App.Dom.onOpenFormCallback === "function") {
                    App.Dom.onOpenFormCallback();
                }
            });
        }, 10);
    }
};

Dom.prototype.getGlobals = function (val, desc) {
    var r = 0;
    switch (val) {
        case 'empresa':
            var act = $(".empresa_dropdown .empresa-actual").text();
            $(".empresa_dropdown ul.dropdown-menu li a div").each(function () {
                if ($(this).text() === act) {
                    if (desc) {
                        r = {
                            'val': atob($(this).closest('a').attr('data-v')),
                            'desc': ($(this).closest('a').text()).trim()
                        };
                    } else {
                        r = atob($(this).closest('a').attr('data-v'));
                    }
                }
            });
            return r;
        case 'periodo':
            var act = $(".periodos_dropdown .periodo-actual").text();
            $(".periodos_dropdown ul.dropdown-menu li a div").each(function () {
                if ($(this).text() === act) {
                    if (desc) {
                        r = {
                            'val': atob($(this).closest('a').attr('data-v')),
                            'desc': ($(this).closest('a').text()).trim()
                        };
                    } else {
                        r = atob($(this).closest('a').attr('data-v'));
                    }
                }
            });
            return r;
        case 'cnegocio':
            var act = $(".cnegocios_dropdown .cnegocio-actual").text();
            $(".cnegocios_dropdown ul.dropdown-menu li a div").each(function () {
                if ($(this).text() === act) {
                    if (desc) {
                        r = {
                            'val': atob($(this).closest('a').attr('data-v')),
                            'desc': ($(this).closest('a').text()).trim()
                        };
                    } else {
                        r = atob($(this).closest('a').attr('data-v'));
                    }
                }
            });
            return r;
        default:
            return r;
    }
};

Dom.prototype.strPad = function (input, pad_length, pad_string, pad_type) {
    var half = '', pad_to_go;

    var str_pad_repeater = function (s, len) {
        var collect = '',
            i;

        while (collect.length < len) {
            collect += s;
        }
        collect = collect.substr(0, len);

        return collect;
    };

    input += '';
    pad_string = pad_string !== undefined ? pad_string : ' ';

    if (pad_type !== 'STR_PAD_LEFT' && pad_type !== 'STR_PAD_RIGHT' && pad_type !== 'STR_PAD_BOTH') {
        pad_type = 'STR_PAD_RIGHT';
    }
    if ((pad_to_go = pad_length - input.length) > 0) {
        if (pad_type === 'STR_PAD_LEFT') {
            input = str_pad_repeater(pad_string, pad_to_go) + input;
        } else if (pad_type === 'STR_PAD_RIGHT') {
            input = input + str_pad_repeater(pad_string, pad_to_go);
        } else if (pad_type === 'STR_PAD_BOTH') {
            half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
            input = half + input + half;
            input = input.substr(0, pad_length);
        }
    }

    return input;
};

Dom.prototype.reloadNavBar = function () {
    $.ajax({
        type: "POST",
        url: BASE_URL_JS + "index/verify/topswitch",
        data: "_t=all&ide=" + btoa(App.Dom.getGlobals('empresa')) + "&idc=" + btoa(App.Dom.getGlobals('cnegocio')) + "&idp=" + btoa(App.Dom.getGlobals('periodo')),
        beforeSend: function (request) {
            request.setRequestHeader("XJENGINE_XWORK", 'V0.1');
            request.setRequestHeader("Dom_METHOD", 'JQUERY');
        }
    }).done(function (data) {
        try {
            var r = JSON.parse(data);
            if (r.response === JSONTRUE) {
                var $target = $('.empresa_dropdown a[data-v="' + btoa(App.Dom.getGlobals('empresa')) + '"]>div');
                $('.empresa-actual').html($target.text());
                var cn = $('.cnegocios_dropdown');
                cn.find('.cnegocio-actual').html(r.data.CURRENT_CNEGOCIO);
                cn.find('.dropdown-menu').html(r.data.ALL_CNEGOCIOS_DROPDOWN);
                var cn = $('.periodos_dropdown');
                cn.find('.periodo-actual').html(r.data.CURRENT_PERIODO);
                cn.find('.dropdown-menu').html(r.data.ALL_PERIODOS_DROPDOWN);

                App.Dom.switchs();
                if (parseInt(r.data.VERIFY_E) === 1 || parseInt(r.data.VERIFY_CN) === 1 || parseInt(r.data.VERIFY_P) === 1) {
//                    document.location.reload();
                }
            } else {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar la empresa</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
            }
            xmodal.hide();
        } catch (e) {
            $.gritter.add({
                title: "Error",
                text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar la empresa</i>",
                class_name: 'growl-danger',
                sticky: false,
                time: 2000
            });
            xmodal.hide();
        }
    }).error(function () {
        $.gritter.add({
            title: "Error",
            text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar la empresa</i>",
            class_name: 'growl-danger',
            sticky: false,
            time: 2000
        });
        xmodal.hide();
    });
};

Dom.prototype.reloadFrame = function () {
    $(document).trigger('hashchange');
};

Dom.prototype.drwdom = function () {
    $('select[data-drwdom]').each(function () {
        var o = $(this);
        var dta = o.attr('data-drwdom');
        var comp = o.closest('.bs-component');
        comp.find('label:not(.control-label)').remove();
        comp.append('<label for="' + o.attr('name') + '" class="warning"><span class="fa fa-spinner xrotate"></span> Cargando...</label>');
        $.ajax({
            type: "POST",
            url: BASE_URL_JS + "index/drawdom/" + dta,
            data: "a=" + parseInt((Math.random(2, 4) * Math.pow(10, 8))),
            beforeSend: function (request) {
                request.setRequestHeader("XJENGINE_XWORK", 'V0.1');
                request.setRequestHeader("Dom_METHOD", 'JQUERY');
            }
        }).done(function (data) {
            try {
                var r = JSON.parse(data);
                if (r.response === JSONTRUE) {
                    o.html(r.data.OPT).val(null);
                    if (o.is('select[class*=chosen-]')) {
                        o.trigger('chosen:updated');
                    }
                    comp.find('label:not(.control-label)').remove();
                } else {
                    comp.find('label:not(.control-label)').remove();
                }
            } catch (e) {
                comp.find('label:not(.control-label)').remove();
            }
        }).error(function () {
            comp.find('label:not(.control-label)').remove();
        });
    });
};


Dom.prototype.switchs = function () {
    $('.empresa_dropdown li.active a').bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
    });
    $('.empresa_dropdown li:not(.active) a').bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var h = $(this).attr('data-v');
        xmodal.show(null, "¿Desea cambiar esta Empresa?", function () {
            $.ajax({
                type: "POST",
                url: BASE_URL_JS + "index/verify/topswitch",
                data: "ide=" + h + "&_t=empresa",
                beforeSend: function (request) {
                    request.setRequestHeader("XJENGINE_XWORK", 'V0.1');
                    request.setRequestHeader("Dom_METHOD", 'JQUERY');
                }
            }).done(function (data) {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        try {
                            if (App.oTable && App.oTable.is(':visible')) {
                                App.oTable.fnDraw();
                                App.Dom.drwdom();
                            }
                            App.Dom.closeForm();
                        } catch (e) {
                        }
                        setTimeout(function () {
                            if (App.Dom.onSwitchChangeCallback && typeof App.Dom.onSwitchChangeCallback === "function") {
                                App.Dom.onSwitchChangeCallback('empresa');
                            }
                        }, 1);
                        $.gritter.add({
                            title: "OK!",
                            text: "<i class='fa fa-clock-o'></i> <i>Se ha cambiado correctamente la empresa</i>",
                            class_name: 'growl-success',
                            sticky: false,
                            time: 2000
                        });
                        var $target = $('.empresa_dropdown a[data-v="' + h + '"]>div');
                        $('.empresa-actual').html($target.text());
                        $('.empresa_dropdown li').removeClass('active');
                        $this.closest('li').addClass('active');
                        var cn = $('.cnegocios_dropdown');
                        cn.find('.cnegocio-actual').html(r.data.CURRENT_CNEGOCIO);
                        cn.find('.dropdown-menu').html(r.data.ALL_CNEGOCIOS_DROPDOWN);
                        var cn = $('.periodos_dropdown');
                        cn.find('.periodo-actual').html(r.data.CURRENT_PERIODO);
                        cn.find('.dropdown-menu').html(r.data.ALL_PERIODOS_DROPDOWN);
                        App.Dom.switchs();
                    } else {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar la empresa</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    }
                    xmodal.hide();
                } catch (e) {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar la empresa</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                    xmodal.hide();
                }
            }).error(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar la empresa</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
                xmodal.hide();
            });
        });
    });

    $('.cnegocios_dropdown li.active a').bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
    });
    $('.cnegocios_dropdown li:not(.active) a').bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var h = $(this).attr('data-v');
        xmodal.show(null, "¿Desea cambiar este Centro de Negocios?", function () {
            $.ajax({
                type: "POST",
                url: BASE_URL_JS + "index/verify/topswitch",
                data: "idc=" + h + "&_t=cnegocios",
                beforeSend: function (request) {
                    request.setRequestHeader("XJENGINE_XWORK", 'V0.1');
                    request.setRequestHeader("Dom_METHOD", 'JQUERY');
                }
            }).done(function (data) {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        try {
                            if (App.oTable && App.oTable.is(':visible')) {
                                App.oTable.fnDraw();
                                App.Dom.drwdom();
                            }
                            App.Dom.closeForm();
                        } catch (e) {
                        }
                        setTimeout(function () {
                            if (App.Dom.onSwitchChangeCallback && typeof App.Dom.onSwitchChangeCallback === "function") {
                                App.Dom.onSwitchChangeCallback('cnegocios');
                            }
                        }, 1);
                        $.gritter.add({
                            title: "OK!",
                            text: "<i class='fa fa-clock-o'></i> <i>Se ha cambiado correctamente el centro de negocios</i>",
                            class_name: 'growl-success',
                            sticky: false,
                            time: 2000
                        });
                        var $target = $('.cnegocios_dropdown a[data-v="' + h + '"]>div');
                        $('.cnegocio-actual').html($target.text());
                        $('.cnegocios_dropdown li').removeClass('active');
                        $this.closest('li').addClass('active');
                        var cn = $('.periodos_dropdown');
                        cn.find('.periodo-actual').html(r.data.CURRENT_PERIODO);
                        cn.find('.dropdown-menu').html(r.data.ALL_PERIODOS_DROPDOWN);
                        App.Dom.switchs();
                    } else {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar el centro de negocios</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    }
                    xmodal.hide();
                } catch (e) {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar el centro de negocios</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                    xmodal.hide();
                }
            }).error(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar el centro de negocios</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
                xmodal.hide();
            });
        });
    });

    $('.periodos_dropdown li.active a').bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
    });
    $('.periodos_dropdown li:not(.active) a').bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var h = $(this).attr('data-v');
        xmodal.show(null, "¿Desea cambiar este Período?", function () {
            $.ajax({
                type: "POST",
                url: BASE_URL_JS + "index/verify/topswitch",
                data: "idp=" + h + "&_t=periodo",
                beforeSend: function (request) {
                    request.setRequestHeader("XJENGINE_XWORK", 'V0.1');
                    request.setRequestHeader("Dom_METHOD", 'JQUERY');
                }
            }).done(function (data) {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        try {
                            if (App.oTable && App.oTable.is(':visible')) {
                                App.oTable.fnDraw();
                                App.Dom.drwdom();
                            }
                            App.Dom.closeForm();
                        } catch (e) {
                        }
                        setTimeout(function () {
                            if (App.Dom.onSwitchChangeCallback && typeof App.Dom.onSwitchChangeCallback === "function") {
                                App.Dom.onSwitchChangeCallback('periodo');
                            }
                        }, 1);
                        $.gritter.add({
                            title: "OK!",
                            text: "<i class='fa fa-clock-o'></i> <i>Se ha cambiado correctamente el período</i>",
                            class_name: 'growl-success',
                            sticky: false,
                            time: 2000
                        });
                        var $target = $('.periodos_dropdown a[data-v="' + h + '"]>div');
                        $('.periodo-actual').html($target.text());
                        $('.periodos_dropdown li').removeClass('active');
                        $this.closest('li').addClass('active');
                        App.Dom.switchs();
                    } else {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar el período</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    }
                    xmodal.hide();
                } catch (e) {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar el período</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                    xmodal.hide();
                }
            }).error(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>No se ha podido cambiar el período</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
                xmodal.hide();
            });
        });
    });
};

Dom.prototype.daemon = function () {
    setInterval(function () {
        var dir = BASE_URL_JS + "index/verify/session";
        var hash = window.location.hash;
        $.ajax({
            type: "POST",
            url: dir,
            beforeSend: function (request) {
                request.setRequestHeader("XJENGINE_XWORK", 'V0.1');
                request.setRequestHeader("Dom_METHOD", 'JQUERY');
            }
        }).done(function (data) {
            try {
                var r = JSON.parse(data);
                if (r.response !== 1) {
                    window.location = BASE_URL_JS + "index/lock" + hash;
                }
            } catch (e) {
                window.location = BASE_URL_JS + "index/lock" + hash;
            }
        });
    }, this.daemonInterval * 1000 * 60);
};

Dom.prototype.addEvent = function (el, eventType, handler) {
    if (el.addEventListener) {
        el.addEventListener(eventType, handler, false);
    } else if (el.attachEvent) {
        el.attachEvent('on' + eventType, handler);
    } else {
        el['on' + eventType] = handler;
    }
};

Dom.prototype.contentLoaded = function (win, fn) {
    var done = false, top = true, doc = win.document, root = doc.documentElement, modern = doc.addEventListener,
        add = modern ? 'addEventListener' : 'attachEvent',
        rem = modern ? 'removeEventListener' : 'detachEvent',
        pre = modern ? '' : 'on',
        init = function (e) {
            if (e.type == 'readystatechange' && doc.readyState != 'complete')
                return;
            (e.type == 'load' ? win : doc)[rem](pre + e.type, init, false);
            if (!done && (done = true))
                fn.call(win, e.type || e);
        },
        poll = function () {
            try {
                root.doScroll('left');
            } catch (e) {
                setTimeout(poll, 50);
                return;
            }
            init('poll');
        };

    if (doc.readyState == 'complete') {
        fn.call(win, 'lazy');
    } else {
        if (!modern && root.doScroll) {
            try {
                top = !win.frameElement;
            } catch (e) {
            }
            if (top)
                poll();
        }
        doc[add](pre + 'DOMContentLoaded', init, false);
        doc[add](pre + 'readystatechange', init, false);
        win[add](pre + 'load', init, false);
    }
};

Dom.prototype.validationExtras = function () {
    $.validator.setDefaults({ignore: []});
    $.validator.addMethod("rut", function (value, element) {
        return this.optional(element) || $.Rut.validar(value);
    }, "Este campo debe ser un rut valido.");

    $.validator.addMethod("maxStock", function (value, element) {
        var pk = $(element).closest('form').find('[name="idinsumo"]').val();
        var lote = $(element).closest('form').find('[name="lote"]').val();
        var bod = $('[name=idbodega]').val();
        App.RevisionErrorsDespacho = {
            pk: 0,
            bod: 0,
            lote: 0
        };
        if (pk === "undefined" || !pk || pk === null || pk + "" === "") {
            App.RevisionErrorsDespacho.pk = 1;
            return false;
        } else {
            App.RevisionErrorsDespacho.pk = 0;
        }
        if (bod === "undefined" || !bod || bod === null || bod + "" === "") {
            App.RevisionErrorsDespacho.bod = 1;
            return false;
        } else {
            App.RevisionErrorsDespacho.bod = 0;
        }
        if (lote === "undefined" || !lote || lote === null || lote + "" === "") {
            App.RevisionErrorsDespacho.lote = 1;
            return false;
        } else {
            App.RevisionErrorsDespacho.lote = 0;
        }

        var dir = BASE_URL_JS + "index/verify/max_stock";
        var data = "_v=true&_k=" + pk + "&_b=" + bod + "&_l=" + lote;
        App.tableDetalleIDInsumo = pk;
        var transaction = new App.sJax('POST', dir, data, false);
        var response = transaction.getReturn();
        try {
            var r = JSON.parse(response);
            var cant = r.data.cant;
            App.cantMaxStockRevisionReturn = cant;
            var cantFinal = parseInt(cant) - parseInt((typeof App.Dom.tableDetalleCounterCant[pk] !== "undefined") ? App.Dom.tableDetalleCounterCant[pk] : 0);
            return (parseInt(value) <= cantFinal);
        } catch (e) {
            return true;
        }

    }, function () {
        if (App.RevisionErrorsDespacho.pk === 1) {
            return "Debe seleccionar un insumo";
        }
        if (App.RevisionErrorsDespacho.bod === 1) {
            return "Debe seleccionar una bodega";
        }
        if (App.RevisionErrorsDespacho.lote === 1) {
            return "Debe escribir un lote válido";
        }
        var cantFinal = parseInt(App.cantMaxStockRevisionReturn) - parseInt((typeof App.Dom.tableDetalleCounterCant[App.tableDetalleIDInsumo] !== "undefined") ? App.Dom.tableDetalleCounterCant[App.tableDetalleIDInsumo] : 0);
        if (cantFinal === 0) {
            return "El lote ingresado no existe o no posee Stock";
        }
        return "La cantidad no puede exceder a " + cantFinal;
    });

    $.validator.addMethod("notEqual", function (value, element, param) {
        return $('[name="' + param + '"]').val() !== value;
    }, "Porfavor seleccione un valor distinto");

    $.validator.addMethod("noRepeat", function (value, element) {
        var v = value;
        var pk = $(element).closest('form').find('[class^=pk_form]')[0].value;
        var params = $(element).attr('data-verificable');
        if (v === "undefined" || params === "undefined" || pk === "undefined" || !v || !pk || !params || v === null || pk === null || params === null) {
            return true;
        } else {
            var dir = BASE_URL_JS + "index/verify/no_repeat";
            var data = "_v=" + v + "&_k=" + pk + "&_p=" + params;
            var transaction = new App.sJax('POST', dir, data, false);
            var response = transaction.getReturn();
            try {
                var r = JSON.parse(response);
                return r.response === 1;
            } catch (e) {
                return true;
            }
        }
    }, "Este valor ya está en la base de datos.");

    $.validator.addMethod("datecompare", function (value, element) {
        try {
            var begin;
            var end;
            var params = $(element).attr('data-datecompare');
            var p = params.split(',');
            var element_compare = '[name=' + p[0] + ']';
            var init = p[1];
            var clean_error = p[2];

            if (init === 'true') {
                begin = value;
                end = $(element_compare).val();
            } else {
                begin = $(element_compare).val();
                end = value;
            }
            if (begin === end) {
                return true;
            } else {
                var dateStart = new Date(begin);
                var dateEnd = new Date(end);
                if (dateStart >= dateEnd) {
                    return false;
                } else {
                    if (clean_error === 'true') {
                        $(element).closest('section').removeClass('has-error').find('label.error').remove();
                        $(element_compare).closest('section').removeClass('has-error').find('label.error').remove();
                        $(element).closest('section').removeClass('has-error').find('.bs-component label.error').remove();
                        $(element_compare).closest('section').removeClass('has-error').find('.bs-component label.error').remove();
                    }
                    return true;
                }
            }
        } catch (e) {
            return false;
        }
    }, "La fecha ingresada no es valida");

    $.validator.addMethod("lote", function (value, element) {
        var v = value;
        var bod = $('[name=idbodega]').val();
        App.RevisionErrorsLote = {
            value: 0,
            bod: 0
        };
        if (v === "undefined" || !v || v === null) {
            App.RevisionErrorsLote.value = 1;
            return false;
        } else {
            App.RevisionErrorsLote.value = 0;
        }
        if (bod === "undefined" || !bod || bod === null) {
            App.RevisionErrorsLote.bod = 1;
            return false;
        } else {
            App.RevisionErrorsLote.bod = 0;
        }

        var dir = BASE_URL_JS + "index/verify/lote";
        var data = "_v=" + v + "&_b=" + bod;
        var transaction = new App.sJax('POST', dir, data, false);
        var response = transaction.getReturn();
        try {
            var r = JSON.parse(response);
            return r.response === 1;
        } catch (e) {
            return true;
        }
    }, function () {
        if (App.RevisionErrorsDespacho.v === 1) {
            return "Debe ingresar un lote";
        }
        if (App.RevisionErrorsDespacho.bod === 1) {
            return "Debe seleccionar una bodega";
        }
        return "El lote ingresado NO existe ";
    });

    $.validator.addMethod("email", function (value) {
        return (/[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,5}/).test(value.trim()) ? true : null;
    }, "Debe ingresar un email válido");
};

Dom.prototype.dataTable = function (url, order, objt) {
    order = (typeof order === "object") ? order : [[0, "desc"]];
    objt = (typeof objt === "undefined") ? $('[data-tablemain]') : objt;
    var obj = {
        responsive: true,
        "oLanguage": {
            "sUrl": BASE_URL_JS + "layout/cube/js/datatables-es.json"
        },
        "bProcessing": true,
        "bServerSide": true,
        "bRetrieve": true,
        "sAjaxSource": url,
        "fnInitComplete": function (oSettings, json) {
            $(".dataTables_filter").addClass('pull-right');
            $("#primal-table_paginate").addClass('pull-right');
        }
    };
    if (order) {
        obj.order = order;
    }
    App.oTable = objt.dataTable(obj);
};

Dom.prototype.editStandard = function (config) {
    App.Dom.closeForm(function () {
        blackout.show('Espere...', function () {
            $.ajax({
                type: "POST",
                url: config.url,
                data: "id=" + config.id
            }).done(function (data) {
                try {
                    var arr_data = JSON.parse(data);
                    var all = arr_data.data.data;
                    for (var i in all) {
                        $("[name=" + i + "]:not(:file)").val(all[i]).trigger("chosen:updated");
                    }
                    $('.form-title').html((typeof App.Dom.formTitleEdit !== "undefined") ? App.Dom.formTitleEdit : "Agregar");
                    $(".pk_form").val(config.id);
                    blackout.hide(function () {
                        $("[data-maincontainerform]").slideDown();
                        if (typeof config.callback === "function") {
                            config.callback(arr_data);
                        }
                    });
                } catch (e) {
                    console.log(e);
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                        if (typeof config.fallback === "function") {
                            config.fallback();
                        }
                    });
                }
            }).error(function () {
                blackout.hide(function () {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 4000
                    });
                    if (typeof config.fallback === "function") {
                        config.fallback();
                    }
                });
            });
        });
    });
};

Dom.prototype.deleteStandard = function (config) {
    App.Dom.closeForm(function () {
        xmodal.show(null, App.Dom.prompDeleteMessage, function () {
            blackout.show('Espere...', function () {
                $.ajax({
                    type: "POST",
                    url: config.url,
                    data: "id=" + config.id
                }).done(function (data) {
                    try {
                        var r = JSON.parse(data);
                        if (r.response === JSONTRUE) {
                            App.oTable.fnDraw();
                            $.gritter.add({
                                title: "OK!",
                                text: "<i class='fa fa-clock-o'></i> <i>" + App.Dom.deleteMessageSuccess + "</i>",
                                class_name: 'growl-success',
                                sticky: false,
                                time: 2000
                            });
                            blackout.hide(function () {
                                if (typeof config.callback === "function") {
                                    config.callback(r);
                                }
                            });
                        } else {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>" + App.Dom.deleteMessageFail + "</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                            blackout.hide(function () {
                                if (typeof config.fallback === "function") {
                                    config.fallback();
                                }
                            });
                        }
                        xmodal.hide();
                    } catch (e) {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>" + App.Dom.deleteMessageFail + "</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                        blackout.hide(function () {
                            if (typeof config.fallback === "function") {
                                config.fallback();
                            }
                        });
                        xmodal.hide();
                    }
                }).error(function () {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>" + App.Dom.deleteMessageFail + "</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                    blackout.hide(function () {
                        if (typeof config.fallback === "function") {
                            config.fallback();
                        }
                    });
                    xmodal.hide();
                });
            });
        });
    });
};

Dom.prototype.bloquear = function () {
    var h = window.location.hash;
    window.location = BASE_URL_JS + 'index/lock/' + h;
};

Dom.prototype.logout = function () {
    var h = window.location.hash.replace(/^#/, '');
    window.location = BASE_URL_JS + 'index/login/logout/' + h;
};

Dom.prototype.verifyChecks = function () {
    var $block = $('[data-seleccionablemin="1"] input[type="checkbox"]'), status = false;
    $block.each(function (i, e) {
        if ($(this).is(':checked')) {
            status = true;
        }
    });

    if (!status) {
        $.gritter.add({
            title: "Error",
            text: "<i class='fa fa-clock-o'></i> <i>Debe al menos seleccionar alguna de las opciones</i>",
            class_name: 'growl-danger',
            sticky: false,
            time: 6000
        });
    }
    return status;
};
Dom.prototype.dataTableError = function (c) {
    try {
        var t = $(App.oTable[0]);
        var $hc = t.find('>thead tr th').length;
        var $b = t.find('tbody');
        var msg = '<tr class="odd"><td valign="top" colspan="' + $hc + '" class="dataTables_empty">Error de Conexión con el Servidor</td></tr>';
        $b.html(msg);
    } catch (e) {
        console.err(e);
        console.err(c);
    }
};

Dom.prototype.sumDate = function (d, fh, format) {
    var f = (format) ? true : false;
    try {
        var Fecha = new Date(fh);
        if (f) {
            var sFecha = fecha || (Fecha.getFullYear() + "/" + (Fecha.getMonth() + 1) + "/" + Fecha.getDate());
        } else {
            var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() + 1) + "/" + Fecha.getFullYear());
        }
        var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
        var aFecha = sFecha.split(sep);
        var fecha = aFecha[2] + '/' + aFecha[1] + '/' + aFecha[0];
        fecha = new Date(fecha);
        fecha.setDate(fecha.getDate() + parseInt(d));
        var anno = fecha.getFullYear();
        var mes = fecha.getMonth() + 1;
        var dia = fecha.getDate();
        mes = (mes < 10) ? ("0" + mes) : mes;
        dia = (dia < 10) ? ("0" + dia) : dia;
        var dateFinal;
        sep = "-";
        dateFinal = anno + sep + mes + sep + dia;
        return (dateFinal);
    } catch (e) {
        return e;
    }
};

Dom.prototype.numberFormat = function (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
};

Dom.prototype.getNowtDate = function () {
    var d = new Date();
    var m = (d.getMonth() + 1);
    var mes = ((m >= 10) ? m : '0' + m);
    return d.getFullYear() + "-" + mes + "-" + ((d.getDate() >= 10) ? d.getDate() : '0' + d.getDate());
};

Dom.prototype.setNowtDate = function (e) {
    e.val(this.getNowtDate());
};

Dom.prototype.getNowHour = function () {
    var d = new Date();
    return ((d.getHours() >= 10) ? d.getHours() : '0' + d.getHours()) + ':' + ((d.getMinutes() >= 10) ? d.getMinutes() : '0' + d.getMinutes());

};
Dom.prototype.setNowHour = function (e) {
    e.val(this.getNowHour());
};


/**
 * No asynchronous AJAX
 * @returns {SJAX}
 */
var SJAX = function (method, dir, params, async) {
    if (typeof async === "undefined") {
        async = false;
    }
    var oXML = this.Obj();
    oXML.open(method, dir, async);
    if (method == "POST") {
        oXML.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    }
    oXML.setRequestHeader('XJENGINE_XWORK', 'V0.1');
    oXML.setRequestHeader('Dom_METHOD', 'SJAX');
    oXML.send(params);
    this.response = oXML.responseText;
};

SJAX.prototype.Obj = function () {
    var obj;
    if (window.XMLHttpRequest) {
        obj = new XMLHttpRequest();
    } else {
        try {
            obj = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
            alert('El navegador utilizado no está soportado');
        }
    }
    return obj;
};

SJAX.prototype.getReturn = function () {
    return this.response;
};

var xLoader = function () {

};

xLoader.prototype.loadCSS = function (filename, callback) {
    if (this.cssExists(filename)) {
        if (typeof callback === "function") {
            callback();
        }
        return;
    }
    var fileref = document.createElement("link"), loaded;
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", filename);
    if (callback) {
        fileref.onreadystatechange = fileref.onload = function () {
            if (!loaded) {
                if (typeof callback === "function")
                    callback();
            }
            loaded = true;
        };
    }
    document.getElementsByTagName("head")[0].appendChild(fileref);
    return this;
};

xLoader.prototype.loadJS = function (filename, callback) {
    if (this.jsExists(filename)) {
        if (typeof callback === "function") {
            callback();
        }
        return;
    }
    var fileref = document.createElement("script"), loaded;
    fileref.setAttribute("type", "text/javascript");
    fileref.setAttribute("src", filename);
    if (typeof callback === "function") {
        fileref.onreadystatechange = fileref.onload = function () {
//            if (!loaded)
            callback();
            loaded = true;
        };
    }
    document.getElementsByTagName("head")[0].appendChild(fileref);

    return this;
};

xLoader.prototype.load = function (filename, callback) {
    var parts = filename.match(/.*\.(.*)/);
    if (parts.length >= 2) {
        var ext = parts[1];
        switch (ext) {
            case 'css':
                return this.loadCss(filename, callback);
            case 'js':
                return this.loadJS(filename, callback);
            default:
                console.warn('No se ha podido determinar el tipo para ' + filename + '.\nUtilice una carga específica.');
        }
    }
};

xLoader.prototype.cssExists = function (filename) {
    var css = document.getElementsByTagName("link");
    for (var i = 0; i < css.length; i++)
        if (filename === css[i].attributes.href.nodeValue)
            return true;
    return false;
};

xLoader.prototype.unloadCss = function (regex, callback) {
    var css = document.getElementsByTagName("link");
    var parent = document.getElementsByTagName("head")[0];
    for (var i = 0; i < css.length; i++) {
        if (css[i].attributes.href.nodeValue.match(regex)) {
            if (callback) {
                if (typeof callback === "function")
                    callback(regex, css[i].attributes.href.nodeValue);
            }
            parent.removeChild(css[i]);
        }
    }
};

xLoader.prototype.jsExists = function (filename) {
    var js = document.getElementsByTagName("script");
    for (var i = 0; i < js.length; i++)
        if (filename === js[i].src)
            return true;
    return false;
};

var xSelectors = function () {
};

xSelectors.prototype.byId = function (_in) {
    _in || (_in = null);
    return document.getElementById(_in);
};

xSelectors.prototype.queryAll = function (_in) {
    _in || (_in = null);
    return document.querySelectorAll(_in);
};

xSelectors.prototype.queryFirst = function (_in) {
    _in || (_in = null);
    return document.querySelectorAll(_in)[0];
};

xSelectors.prototype.nameFirst = function (_in, _form) {
    _form || (_form = null);
    _in || (_in = null);
    if (_form)
        return this.byId(_form).querySelectorAll('[name="' + _in + '"]')[0];
    else
        return document.getElementsByName(_in)[0];
};

Dom.prototype.selectors = new xSelectors();

var TM = function (t, m, s) {
    this.type = (t === '') ? 'error' : t;
    this.msg = (m === '') ? 'Error desconocido' : m;
    this.time = 6000;
    this.sticky = (typeof (s) === 'undefined') ? false : s;
};

TM.prototype.show = function (time) {
    time || (time = 6000);
    this.time = time;
    switch (this.type) {
        case 'error':
        case 'Error':
            var a = {
                title: "Error",
                text: "<i class='fa fa-exclamation'></i> <i>" + this.msg + "</i>",
                class_name: "growl-danger",
                sticky: this.sticky,
                time: this.time
            };
            break;
        case 'alerta':
        case 'alert':
            var a = {
                title: "Alerta",
                text: "<i class='fa fa-warning'></i> <i>" + this.msg + "</i>",
                class_name: "growl-alert",
                sticky: this.sticky,
                time: this.time
            };
            break;
        case 'correcto':
        case 'ok':
        case 'OK':
        case 'OK!':
            var a = {
                title: "Correcto",
                text: "<i class='fa fa-check-square-o'></i> <i>" + this.msg + "</i>",
                class_name: "growl-success",
                sticky: this.sticky,
                time: this.time
            };
            break;
        case 'info':
            var a = {
                title: "Info",
                text: "<i class='fa fa-info'></i> <i>" + this.msg + "</i>",
                class_name: "growl-info",
                sticky: this.sticky,
                time: this.time
            };
            break;
        default:
            var a = {
                title: "Error",
                text: "<i class='fa fa-exclamation'></i> <i>" + this.msg + "</i>",
                class_name: "growl-danger",
                sticky: this.sticky,
                time: this.time
            };
            break;
    }
    blackout.hide();
    $.gritter.add(a);
};


var App = {};
var runall = function (cback) {
    App = {};
    App.sJax = SJAX;
    App.ONSESSION = true;
    App.Dom = new Dom();
    App.oTable = null;
    App.Dom.daemon();
    App.Dom.switchs();
    App.loader = new xLoader();
    App.FLEXMOSTER_LICENCE_KEY = 'Z519-141PJ1-1B10-131W-343M-0B0Y-2B10-1B18-0H0Y-1H0P-101N';
    if (typeof cback === "function") {
        cback();
    }
};
runall(function () {
    App.Dom.onLaunchAppRun();
});


