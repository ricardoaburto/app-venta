/**
 * Copyright ©2014-2016 by Marcel Rojas
 */
'use strict';

/**
 * Sockets for XWork
 * @returns {xSocket}
 */
var xSocket = (function () {
    var settings = {
        host: "",
        onopen: function () {
        },
        onmessage: function () {
        },
        onclose: function () {
        }
    };
    var instance = null;

    function SocketException(mensaje) {
        this.mensaje = mensaje;
        this.nombre = "SocketException";
    };

    var initialize = function () {
        if(!("WebSocket" in window)){
            throw new SocketException('WebSocket not supported');
        }
        if (instance === null) {
            if (settings.host === "") {
                throw new SocketException('Host Not Defined');
            }
            instance = new WebSocket(settings.host.replace(/^(http|https):\/\//gi,"ws://"));
            instance.onopen = settings.onopen;
            instance.onmessage = settings.onmessage;
            instance.onclose = settings.onclose;
            instance.onerror = settings.onerror;
        }
    };
    
    var send = function(message){
        if (instance === null) {
            throw new SocketException('Uninitialized Socket');
        }
        instance.send(message);
    };
    
    var disconnect = function(){
        if (instance === null) {
            throw new SocketException('Uninitialized Socket');
        }
        instance.close();
    };

    return {
        create: function (s) {
            for (var name in s)
                settings[name] = s[name];
            return this;
        },
        init: function () {
            initialize();
            return this;
        },
        send: function (message) {
            send(message);
            return this;
        },
        disconnect: function () {
            disconnect();
            return this;
        }
    };
}());

