/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var xmodal = function() {
    var loadInnerModal = function(callback) {
        callback || (callback = function() {
        });
        var bot = document.getElementById('xmodalHandler');
        bot.innerHTML = '<div class="heading"></div>\n\
                            <div class="content">\n\
                                <p class="bodymodal">\n\
                                </p>\n\
                                <p class="buttons">\n\
                                    <button type="button" class="btn btn-w-m btn-danger canceldeal">Cancelar</button>\n\
                                    <button type="button" class="btn btn-w-m btn-primary acceptdeal">Aceptar</button>\n\
                                </p>\n\
                            </div>';
        callback();
    };

    var init = function(callback) {
        var body = document.getElementsByTagName('body')[0];
        if (!document.getElementById('reveal-xmodal-bg')) {
            var xm = document.createElement('div');
            xm.id = 'reveal-xmodal-bg';
            xm.addEventListener('click',function(){xmodal.hide();});
            body.appendChild(xm);
//            xm.onload = function() {
                var bo = document.createElement('div');
                bo.id = 'xmodalHandler';
                body.appendChild(bo);
                bo.onload = loadInnerModal(callback);
//            };
        } else if (!document.getElementById('xmodalHandler')) {
            var bo = document.createElement('div');
            bo.id = 'xmodalHandler';
            body.appendChild(bo);
            bo.onload = loadInnerModal(callback);
        } else {
            loadInnerModal(callback);
        }
        return true;
    };
    
    var hideModal = function(callback){
        $("#xmodalHandler, #reveal-xmodal-bg").fadeOut(function(){callback();});
    };
    return {
        show: function(title, body, acceptdeal, canceldeal, onopen) {
            init(function() {
                canceldeal || (canceldeal = function(){xmodal.hide();});
                onopen || (onopen = function(){});
                if(title){
                    $("#xmodalHandler .heading").html(String(title));
                } else {
                    $("#xmodalHandler .heading").remove();
                    $("#xmodalHandler .content").addClass('fullradius');
                }
                $("#xmodalHandler .content p.bodymodal").html(String(body));
                $("#xmodalHandler .acceptdeal").bind('click', acceptdeal);
                $("#xmodalHandler .canceldeal").bind('click', canceldeal);
                $("#xmodalHandler, #reveal-xmodal-bg").fadeIn(function(){
                    onopen();
                });
            });
        },
        hide: function(callback){
            callback || (callback = function(){});
            hideModal(callback);
        }
    };
}();
