<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Response extends Controller
{
    const JSONTRUE = '{"response":1}';
    const JSONFALSE = '{"response":0}';
    const JSONNULL = '{}';

    /**
     * Permite parsear una respuesta JSON
     * @param array $data array con los datos a parsear
     * @return String Cadena JSON parseada
     */
    public static function JSONDATA(array $data) {
        $obj = new \stdClass();
        $obj->response = 1;
        $obj->data = $data;
        return json_encode($obj);
    }

    /**
     * Permite parsear una respuesta JSON
     * @param array $data array con los datos a parsear
     * @return String Cadena JSON parseada
     */
    public static function JSONDATACTX(array $data, $ctx) {
        $obj = new \stdClass();
        $obj->response = 1;
        $obj->data = $data;
        $obj->context = $ctx;
        return json_encode($obj);
    }

    /**
     * Permite parsear un error o excepcion para devolverla como una cadena JSON
     * @param mixed $traceMessage error o Excepcion a parsear
     * @param int $errno numero de error
     * @return String Cadena JSON parseada
     */
    public static function JSONTRACEERROR($traceMessage, $errno = 0) {
        $obj = new \stdClass();
        $obj->response = 0;
        if (!($traceMessage instanceof \Exception)) {
            $obj->message = (string)$traceMessage;
            $obj->errno = (int)$errno;
        } else {
            $obj->message = (string)$traceMessage->getMessage();
            $obj->errno = (int)$traceMessage->getCode();
        }
        return json_encode($obj);
    }

    public static function HEADER($code, $fnc) {
        http_response_code($code);
        die($fnc);
    }

}
