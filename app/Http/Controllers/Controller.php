<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function now($hour = TRUE) {
        if ($hour) {
            return date('Y-m-d H:i:s');
        } else {
            return date('Y-m-d');
        }
    }

    public function getSelectOptions($obj, $value, $name, $placeholder = 'Seleccione una opcion', $special = false, $defvalue = 0, $nospace = false, $selectedindraw = null)
    {
        if (($placeholder AND FALSE) OR $special) {
            $r = "<option value=\"$defvalue\">" . $placeholder . "</option>";
        } else {
            $r = "";
        }
        $selected = 1;
        foreach ($obj as $i => $ares) {
            if (!is_null($selectedindraw) && $selectedindraw == $ares->{$value}) {
                $selected = 0;
            }
            $r .= '<option value="' . $ares->{$value} . '" ' . (($selected == 0) ? " selected=\"selected\" " : '') . '>';
            $selected++;
            if (is_array($name)) {
                foreach ($name as $key => $na) {
                    if (isset($ares->{$na})) {
                        $r .= $ares->{$na};
                    } else {
                        $r .= $na;
                    }
                    if (!$nospace) {
                        $r .= ' ';
                    }
                }
            } else {
                $r .= $ares->{$name};
            }
            $r .= '</option>';
        }
        return $r;
    }

}
