<?php

namespace App\Http\Controllers;

use App\Models\DetalleModel;
use App\Models\VentaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $venta = DB::table('venta')
            ->join('cliente', function ($join) {
                $join->on('cliente.id', '=', 'venta.cliente_id');
            })->where(DB::raw('md5(venta.id)'), $request->id)->select('venta.id as id','venta.iva as iva','venta.descuento as descuento', 'cliente.nombre as cliente', 'venta.fecha as fecha', 'venta.total as total')->first();


        $detalle = DB::table('detalle')
            ->join('producto', function ($join) {
                $join->on('producto.id', '=', 'detalle.producto_id');
            })->where('detalle.venta_id', '=', $venta->id)
            ->select( 'producto.nombre as producto','detalle.cantidad','detalle.subtotal as subtotal')->get();

        $dubtotalventa = 0;
        foreach ($detalle as $item) {
            $dubtotalventa+=$item->subtotal;
        }


        $data = [
            'date' => date('m/d/Y'),
            'cliente' => $venta->cliente,
            'fecha' => $venta->fecha,
            'subtotal' =>number_format($dubtotalventa, 0, ',', '.'),
            'descuento' => $venta->descuento,
            'iva' =>number_format($venta->iva, 0, ',', '.') ,
            'total' =>number_format($venta->total, 0, ',', '.') ,
            'detalle' => $detalle->toArray() ,
        ];

        $pdf = PDF::loadView('boletaPDF', $data);
        $pdf->getDomPdf()->getOptions()->set('enable_php', true);
        return $pdf->download('boleta_'.date('m/d/Y').'.pdf');
    }
}
