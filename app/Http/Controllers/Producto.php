<?php

namespace App\Http\Controllers;

use App\Models\DetalleModel;
use App\Models\ProductoModel;
use App\Models\VentaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class Producto extends Controller
{
    public function index()
    {
        return view('sistema/producto.producto');
    }

    public function edit($id)
    {
        $producto = DB::table('producto')->where(DB::raw('md5(id)'), $id)->first();
        return Response::JSONDATA(['data' => $producto]);
    }

    public function getTable()
    {
        $aColumns = array('id', 'nombre', 'precio','cantidad');
        $noShowNum = 1;
        $sIndexColumn = "id";
        $aWhere = [];
        $iTotal = DB::table('producto')
            ->select(DB::raw('count(id) as id'))
            ->where($aWhere)
            ->get();


        $sLimit = NULL;
        $sOrder = NULL;

        $m = DB::table('producto')
            ->select($aColumns);

        if (isset($_GET['iSortCol_0'])) {
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) + $noShowNum] . " " . addslashes($_GET['sSortDir_' . $i]) . ", ";
                }
            }
            $sOrder = substr_replace($sOrder, "", -2);
            $m->orderByRaw($sOrder);
        }

        if ($_GET['sSearch'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {

                $m->orWhere($aColumns[$i], 'LIKE', '%' . addslashes($_GET['sSearch']) . '%');
            }
        }

        for ($i = 0; $i < count($aColumns); $i++) {
            if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                $m->orWhere($aColumns[$i], 'LIKE', '%' . addslashes($_GET['sSearch_' . $i]) . '%');
            }
        }

        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $data = $m->skip(addslashes($_GET['iDisplayStart']))->take(addslashes($_GET['iDisplayLength']))->get();
        } else {
            $data = $m->get();
        }
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal[0]->id,
            "iTotalDisplayRecords" => $iTotal[0]->id,
            "aaData" => array()
        );

        for ($j = 0; $j < count($data); $j++) {
            $aRow = $data[$j];
            $row = array();
            for ($i = $noShowNum; $i < (count($aColumns)); $i++) {
                $row[] = $aRow->{$aColumns[$i]};
            }

            $row[1] = '<div >' .number_format(str_replace('.','',$aRow->precio) , 0, ',', '.')  .'</div>';

            $r = "";
            $r .= '<div class="btn-group btn-center"  data-dataid="' . md5($aRow->{$sIndexColumn}) . '">
                        <button class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-gear"></i>  <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu fixed-dropdown-menu1 pull-right">
                            <li>
                                <a class="txt-color-green" href="#" onclick="App.editar(\'' . md5($aRow->{$sIndexColumn}) . '\');return false;"><i class="fa fa-edit"></i> Editar</a>
                            </li>';
            $r .= ' <li>
                                <a class="txt-color-red" href="#" onclick="App.eliminar(\'' . md5($aRow->{$sIndexColumn}) . '\');return false;"><i class="fa fa-trash-o"></i> Eliminar</a>
                            </li>';

            $r .= ' </ul>
                      </div>';
            $row[] = $r;

            $output['aaData'][] = $row;
        }
        return json_encode($output);
    }

    public function actualizar()
    {
        try {

            $producto = ProductoModel::where(DB::raw('md5(id)'), $_POST['idproducto']);
            $producto->update(['cantidad' => $_POST['cantidad'], 'nombre' => $_POST['nombre'], 'precio' =>str_replace(',', '.', str_replace('.', '', $_POST['precio']))]);

            return Response::JSONTRUE;
        } catch (Exception $e) {
            return Response::JSONFALSE;
        }

    }

    public function store()
    {

        try {
            $detalle = new ProductoModel();
            $detalle->nombre = $_POST['nombre'];
            $detalle->cantidad = $_POST['cantidad'];
            $detalle->precio = str_replace(',', '.', str_replace('.', '', $_POST['precio']));
            $detalle->save();
            return Response::JSONTRUE;
        } catch (Exception $e) {
            return Response::JSONFALSE;
        }
    }

    public function eliminar()
    {
        try {

            $stockTablaDetalle = DB::table('detalle')
                ->join('producto', function ($join) {
                    $join->on('producto.id', '=', 'detalle.producto_id')
                        ->where(DB::raw('md5(producto.id)'), $_POST['id']);
                })
                ->count();
            if ($stockTablaDetalle === 0) {
                DB::table('producto')->where(DB::raw('md5(id)'), $_POST['id'])->delete();
            }
            return Response::JSONDATA(['data'=>$stockTablaDetalle]);
        } catch (Exception $e) {
            return Response::JSONFALSE;
        }

    }

}
