<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductoModel extends Model
{
    use HasFactory;
    protected $table = 'producto';
    protected $primaryKey = 'id';
    protected $visible = ['nombre','precio','cantidad','id'];
    public $timestamps = false;
}
