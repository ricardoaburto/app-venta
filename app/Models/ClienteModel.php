<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClienteModel extends Model
{
    use HasFactory;
    use HasFactory;
    protected $table = 'cliente';
    protected $primaryKey = 'id';
    protected $visible = ['id','nombre','estado'];
    public $timestamps = false;
}
