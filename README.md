# Laravel App Venta

[![Build Status](https://img.shields.io/travis/gothinkster/laravel-realworld-example-app/master.svg)](https://travis-ci.org/gothinkster/laravel-realworld-example-app) [![Gitter](https://img.shields.io/gitter/room/realworld-dev/laravel.svg)](https://gitter.im/realworld-dev/laravel) [![GitHub stars](https://img.shields.io/github/stars/gothinkster/laravel-realworld-example-app.svg)](https://github.com/gothinkster/laravel-realworld-example-app/stargazers) [![GitHub license](https://img.shields.io/github/license/gothinkster/laravel-realworld-example-app.svg)](https://raw.githubusercontent.com/gothinkster/laravel-realworld-example-app/master/LICENSE)

> ###  Web application on laravel and Jetstream [app-venta](http://137.184.27.35/app/venta).

This repo is functionality complete —  welcome!

----------

# Getting started

## Installation

Please check the official laravel 8 installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/8.x/installation)


Clone the repository

    git clone https://gitlab.com/ricardoaburto/app-venta.git

Switch to the repo folder

    cd app-venta

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run template Jetstream

    npm install
    npm run dev or prod


Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    git clone https://gitlab.com/ricardoaburto/app-venta.git
    cd app-venta
    composer install
    cp .env.example .env
    php artisan key:generate
    npm install
    npm run dev
    php artisan migrate
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan serve


## Dependencies

- [npm](https://docs.npmjs.com/cli/v7/commands/npm) - For install Jetstream
- [nodejs](https://nodejs.org/) - For dependencies >=10.13.0



## Environment variables

- `.env` - Environment variables can be set in this file


    APP_NAME=YOURAPP
    APP_ENV=local
    APP_KEY=APPLICATION_UNIQUE_KEY_DONT_COPY
    APP_DEBUG=true
    APP_URL=http://domain_or_IP

    LOG_CHANNEL=stack

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=YOURDATABASE
    DB_USERNAME=YOURUSER
    DB_PASSWORD=YOURPASSWORD

***Note*** : You can quickly set the database information and other variables in this file and have the application fully working.

----------

# Testing APP

Run the laravel development server

    php artisan serve

The api can now be accessed at

    http://localhost:8000

Crear cuenta para iniciar
    http://localhost:8000/register



