<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});



Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::resource('/app/venta', \App\Http\Controllers\Venta::class)->except(['destroy','store','index']);
    Route::get('/app/venta',  [\App\Http\Controllers\Venta::class, 'index'])->name('app/venta');
    Route::get('/app/table_venta',  [\App\Http\Controllers\Venta::class, 'getTable']);
    Route::get('/app/get',[\App\Http\Controllers\Venta::class, 'show']);
    Route::post('/app/venta/getvalue',[\App\Http\Controllers\Venta::class, 'getValue']);
    Route::post('/app/venta/detalle/add',[\App\Http\Controllers\Venta::class, 'createDetalle']);
    Route::get('/app/edit/{id}',[\App\Http\Controllers\Venta::class, 'edit']);
    Route::post('/app/venta/create',[\App\Http\Controllers\Venta::class, 'store']);
    Route::post('/app/venta/actualizar',[\App\Http\Controllers\Venta::class, 'actualizar']);
    Route::post('/app/venta/eliminar',[\App\Http\Controllers\Venta::class, 'eliminar']);
    Route::get('/app/venta/pdf-boleta/{id}', [\App\Http\Controllers\PDFController::class, 'index']);
    // More routes here



    Route::resource('/app/producto', \App\Http\Controllers\Producto::class)->except(['destroy','store','index','show']);
    Route::get('/app/producto',  [\App\Http\Controllers\Producto::class, 'index'])->name('app/producto');
    Route::get('/app/producto/table_producto',  [\App\Http\Controllers\Producto::class, 'getTable']);
    Route::get('/app/producto/get',[\App\Http\Controllers\Producto::class, 'show']);
    Route::post('/app/producto/create',[\App\Http\Controllers\Producto::class, 'store']);
    Route::post('/app/producto/actualizar',[\App\Http\Controllers\Producto::class, 'actualizar']);
    Route::post('/app/producto/eliminar',[\App\Http\Controllers\Producto::class, 'eliminar']);
    Route::get('/app/producto/edit/{id}',[\App\Http\Controllers\Producto::class, 'edit']);



    Route::resource('/app/cliente', \App\Http\Controllers\Cliente::class)->except(['destroy','store','index','show']);
     Route::get('/app/cliente',  [\App\Http\Controllers\Cliente::class, 'index'])->name('app/cliente');
   Route::get('/app/cliente/table_cliente',  [\App\Http\Controllers\Cliente::class, 'getTable']);
   Route::get('/app/cliente/get',[\App\Http\Controllers\Cliente::class, 'show']);
     Route::post('/app/cliente/create',[\App\Http\Controllers\Cliente::class, 'store']);
     Route::post('/app/cliente/actualizar',[\App\Http\Controllers\Cliente::class, 'actualizar']);
     Route::post('/app/cliente/eliminar',[\App\Http\Controllers\Cliente::class, 'eliminar']);
     Route::get('/app/cliente/edit/{id}',[\App\Http\Controllers\Cliente::class, 'edit']);
    //logs
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

});

