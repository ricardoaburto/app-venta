<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="">
                <div class="content">
                    <div class="container">
                        <section id="content" class="animated fadeIn">
                            <div class="row full-row" data-maincontainerform style="display: none;">
                                <div class="col-lg-12">
                                    <div class="card-box" id="p0" data-panel-title="false" data-panel-fullscreen="false"
                                         data-panel-remove="false" data-panel-collapse="false">
                                        <form class="form-horizontal" action="#" id="primal-form">
                                            <div class="m-b-30">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default bx-shadow-none panel-min-height">
                                                            <div class="panel-heading">
                                                                <h3 class="panel-title" style="color:#797979;">Producto
                                                                </h3>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4">
                                                                        <label class="col-sm-4 control-label">Producto</label>
                                                                        <div class="col-sm-8">
                                                                            <div class="bs-component">
                                                                                <div class="bs-component">
                                                                                    <input type="text" class="form-control" name="nombre" placeholder="Nombre" required="required">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <label class="col-sm-4 control-label">Precio</label>
                                                                        <div class="col-sm-8">
                                                                            <div class="bs-component">
                                                                                <div class="bs-component">
                                                                                    <input type="number" class="form-control text-right" data-numbercharacters
                                                                                           name="precio" value="0" required="required">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <label class="col-sm-4 control-label">Cantidad</label>
                                                                        <div class="col-sm-8">
                                                                            <div class="bs-component">
                                                                                <div class="bs-component">
                                                                                    <input type="number" class="form-control text-right" data-numbercharacters
                                                                                           name="cantidad" value="0" required="required">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <input type="hidden" name="idproducto" id="idproducto" value="0" class="pk_form"/>
                                            <div class="text-right">
                                                <a class="btn btn-inverse waves-effect waves-light" href="#" data-cancel_form>Cancelar</a>
                                                <button class="btn btn-primary waves-effect waves-light"  type="submit">
                                                    Guardar
                                                </button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="row full-row">
                                <div class="col-md-12">
                                    <div class="card-box table-responsive" id="spy2">
                                        <div class="pull-right">
                                            <button type="button" class="btn btn-primary waves-effect waves-light"
                                                    onclick="" data-addnew>Agregar
                                            </button>
                                        </div>
                                        <h4 class="header-title m-b-30">Listado de Productos</h4>
                                        @include('sistema.producto.tableproducto')

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
