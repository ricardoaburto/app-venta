<!DOCTYPE html>
<html lang="en">

<body>
<header class="clearfix">
    <div class="div1">Boleta <span
            style="float: right; font-size: 22px"></span>
    </div>

</header>
<main>
    <h2>Venta</h2>
    <div>
        <table >

            <tbody>

            <tr>
                <td style="font-size: 16px"  colspan="2">Cliente: {{$cliente}}</td>
                <td  style="font-size: 16px"  colspan="2">Fecha: {{$fecha}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div>
        <br>
        <h3>Detalle de Productos</h3>
        <table>
            <thead>
            <tr>
                <th >Producto</th>
                <th>Cantidad</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($detalle as $del)
                <tr>
                <td>{{ $del->producto }}</td>
                <td>{{ $del->cantidad }}</td>
                <td>{{number_format($del->subtotal , 0, ',', '.') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <br>
    <br>
    <table style="width: 30%; float: right;  ">
        <tr>
            <td colspan="2">
                Detalle de Pago
            </td>
        </tr>
        <tr>
            <td>Subtotal:</td>
            <td> {{$subtotal}} </td>
        </tr>
        <tr>
            <td>Descuento:</td>
            <td> {{$descuento}}</td>
        </tr>
        <tr>
            <td>Iva:</td>
            <td> {{$iva}}</td>
        </tr>
        <tr>
            <td>Total:</td>
            <td> {{$total}}</td>
        </tr>
    </table>
</main>
<footer>
    <div>
        <div><span>Fecha: </span> {{$date}}</div>
    </div>
</footer>
</body>
</html>
<style type="text/css">
    .clearfix:after {
        content: "";
        display: table;
        clear: both;
    }
    a {
        color: #5D6975;
        text-decoration: underline;
    }
    body {
        position: relative;
        width: 19cm;
        height: 29.7cm;
        margin: 0 auto;
        color: #001028;
        background: #FFFFFF;
        font-family: Arial, sans-serif;
        font-size: 12px;
        font-family: Arial;
    }
    header {
        padding: 10px 0;
        margin-bottom: 30px;
    }
    #logo {
        text-align: center;
        margin-bottom: 10px;
    }
    #logo img {
        width: 90px;
    }
    .div1 {
        border-top: 1px solid #5D6975;
        border-bottom: 1px solid #5D6975;
        color: #5D6975;
        font-size: 2.4em;
        line-height: 1.4em;
        font-weight: normal;
        margin: 0 0 20px 0;
    }
    #project {
        float: left;
    }
    #project span {
        color: #5D6975;
        text-align: right;
        width: 52px;
        margin-right: 10px;
        display: inline-block;
        font-size: 0.8em;
    }
    #company {
        float: right;
        text-align: right;
    }
    #project div,
    #company div {
        white-space: nowrap;
    }
    table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px;
    }
    table tr:nth-child(2n-1) td {
        background: #F5F5F5;
    }
    table th,
    table td {
        text-align: center;
    }
    table th {
        padding: 5px 20px;
        color: #5D6975;
        border-bottom: 1px solid #C1CED9;
        white-space: nowrap;
        font-weight: normal;
    }
    table .service,
    table .desc {
        text-align: left;
    }
    table td {
        padding: 10px;
        text-align: center;
    }
    table td.service,
    table td.desc {
        vertical-align: top;
    }
    table td.unit,
    table td.qty,
    table td.total {
        font-size: 1.2em;
    }
    table td.grand {
        border-top: 1px solid #5D6975;;
    }
    #notices .notice {
        color: #5D6975;
        font-size: 1.2em;
    }
    footer {
        color: #5D6975;
        width: 100%;
        height: 30px;
        position: absolute;
        bottom: 0;
        border-top: 1px solid #C1CED9;
        padding: 8px 0;
        text-align: center;
    }
</style>
