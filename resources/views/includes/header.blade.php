<!-- Top Bar Start -->
<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            <a href="index.html" class="logo">
            </a>
        </div>
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">
                <div class="pull-left">
                    <button class="button-menu-mobile open-left waves-effect waves-light">
                        <i class="zmdi zmdi-menu"></i>
                    </button>
                    <span class="clearfix"></span>
                </div>

                <form role="search" class="navbar-left app-search pull-left hidden-xs">
                </form>
                <ul class="nav navbar-nav navbar-right pull-right">
                    <li>
                        <!-- Notification -->
                        <div class="notification-box">
                            <ul class="list-inline m-b-0">
                                <li>
                                    <div class="noti-dot">
                                        <span class="dot"></span>
                                        <span class="pulse"></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- End Notification bar -->
                    </li>

                </ul>

            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<!-- Top Bar End -->


<!-- ========== Left Sidebar Start ========== -->

<div class="left side-menu">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 877px;">
        <div class="sidebar-inner slimscrollleft" style="overflow: hidden; width: auto; height: 877px;">
            <!--- Divider -->
            <div id="sidebar-menu">
                <ul>
                    <li class="has_sub active">
                        <a href="/admin/venta" class="waves-effect active"><i class="zmdi zmdi-view-dashboard"></i>
                            <span> Ventas </span> </a>
                    </li>
                    </li>
                    <li class="has_sub">
                        <a href="/admin/venta" class="waves-effect active"><i class="zmdi zmdi-view-dashboard"></i>
                            <span> Clientes </span> </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>

        </div>
        <div class="slimScrollBar"
             style="background: rgb(220, 220, 220); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 877px; visibility: visible;"></div>
        <div class="slimScrollRail"
             style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
    </div>
</div>
<!-- Left Sidebar End -->
